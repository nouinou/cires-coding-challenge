CREATE TABLE IF NOT EXISTS `USER` (
    `id` bigint NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `firstName` varchar(50),
    `lastName` varchar(50),
    `birthDate` date,
    `city` varchar(50),
    `country` varchar(2),
    `avatar` varchar(225),
    `company` varchar(50),
    `jobPosition` varchar(50),
    `mobile` varchar(25),
    `username` varchar(50),
    `email` varchar(50),
    `password` varchar(50),
    `role` varchar(10),
    CONSTRAINT uk_username UNIQUE KEY (username),
    CONSTRAINT uk_email UNIQUE KEY (email)
)