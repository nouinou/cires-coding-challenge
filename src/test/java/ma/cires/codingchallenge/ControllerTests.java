package ma.cires.codingchallenge;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testGenerateUsers() throws Exception {
        mockMvc.perform(get("/api/users/generate?count=10"))
                .andExpect(status().isOk());
    }

    @Test
    void testImportUsers() throws Exception {
        byte[] fileBytes =  mockMvc.perform(get("/api/users/generate?count=10")).andReturn().getResponse().getContentAsByteArray();
        MockMultipartFile file = new MockMultipartFile("users", fileBytes);
                mockMvc.perform(multipart("/api/users/batch").file(file))
                .andExpect(status().isOk());
    }

}
