package ma.cires.codingchallenge;

import ma.cires.codingchallenge.user.core.domain.models.User;
import ma.cires.codingchallenge.user.core.utils.UserGeneratorUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
public class UserGeneratorUtilTests {

    @Test
    void itShouldGenerateAUser() {
        UserGeneratorUtil generator = new UserGeneratorUtil();

        User user = new User(
                generator.name().firstName(),
                generator.name().lastName(),
                generator.date().birthday(),
                generator.address().city(),
                generator.address().countryCode(),
                generator.avatar().image(),
                generator.company().name(),
                generator.job().position(),
                generator.phoneNumber().cellPhone(),
                generator.name().username(),
                generator.internet().safeEmailAddress(),
                generator.internet().password(6, 10, true, true, true),
                generator.role().toString());

        assertThat(user).isNotNull();
        assertThat(user.getUsername()).isNotNull();
        assertThat(user.getEmail()).isNotNull();
        assertThat(user.getPassword()).isNotNull();
    }

}
