package ma.cires.codingchallenge;

import ma.cires.codingchallenge.user.core.domain.models.ImportUserStats;
import ma.cires.codingchallenge.user.core.domain.models.User;
import ma.cires.codingchallenge.user.core.domain.models.UserProfile;
import ma.cires.codingchallenge.user.ports.infrastructure.UserRepositoryImplementation;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
public class UsersRepositoryImplementationTests {

    @Autowired
    private UserRepositoryImplementation userRepositoryImplementation;

    @Test
    void itShouldGenerateUsers() {
        int count = 10;
        List<User> generatedUsers = userRepositoryImplementation.generateUsers(count);

        assertThat(generatedUsers.size()).isEqualTo(count);
    }

    @Test
    void itShouldImportUserList() {
        List<User> userList = userRepositoryImplementation.generateUsers(10);

        // users list can have duplicates. Making them unique
        for(int i = 0; i < userList.size(); i++){
            userList.get(i).setUsername("fakeUsername" + i);
            userList.get(i).setEmail("fake" + i + "@email.fake");
        }

        // Imported 10 out of 10 Users,  no duplicates
        ImportUserStats stats = userRepositoryImplementation.importUserList(userList);
        ImportUserStats expected = new ImportUserStats(10, 10, 0);

        assertThat(stats).isEqualTo(expected);
    }

    @Test
    void itShouldNotImportDuplicatedUsers() {
        List<User> userList = userRepositoryImplementation.generateUsers(10);

        // Setting the first user username the same as the 8th user
        User firstUser = userList.get(0);
        String eighthUserUsername = userList.get(8).getUsername();
        firstUser.setUsername(eighthUserUsername);

        // Setting the 4th user email the same as the third user
        User fourthUser = userList.get(4);
        String thirdUserUsername = userList.get(3).getUsername();
        fourthUser.setUsername(thirdUserUsername);

        ImportUserStats stats = userRepositoryImplementation.importUserList(userList);

        // Imported 8 out of 10 Users, 2 are duplicates
        ImportUserStats expected = new ImportUserStats(10, 8, 2);

        assertThat(stats).isEqualTo(expected);
    }

    @Test
    void itShouldGetUser() {
        List<User> userList = userRepositoryImplementation.generateUsers(10);
        userRepositoryImplementation.importUserList(userList);

        String expectedUsername = userList.get(0).getUsername();
        String expectedEmail = userList.get(9).getEmail();

        UserProfile user1 = userRepositoryImplementation.getUser(expectedUsername);
        UserProfile user2 = userRepositoryImplementation.getUser(expectedEmail);

        assertThat(user1.getUsername()).isEqualTo(expectedUsername);
        assertThat(user2.getEmail()).isEqualTo(expectedEmail);
    }

    @Test
    void itShouldLoadUserByUsername() {
        List<User> userList = userRepositoryImplementation.generateUsers(10);
        userRepositoryImplementation.importUserList(userList);

        String username = userList.get(0).getUsername();

        UserDetails userDetails = userRepositoryImplementation.loadUserByUsername(username);
        String email = userDetails.getUsername();

        String expected = userList.get(0).getUsername();

        assertThat(username).isEqualTo(expected);
    }

    @Test
    void itShouldLoadUserByEmail() {
        List<User> users = userRepositoryImplementation.generateUsers(10);
        userRepositoryImplementation.importUserList(users);

        String expected = users.get(0).getEmail();

        UserDetails userDetails = userRepositoryImplementation.loadUserByUsername(expected);
        String username = userDetails.getUsername();

        assertThat(username).isEqualTo(expected);
    }
}
