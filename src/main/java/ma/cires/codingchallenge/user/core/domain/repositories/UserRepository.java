package ma.cires.codingchallenge.user.core.domain.repositories;

import ma.cires.codingchallenge.user.core.domain.models.ImportUserStats;
import ma.cires.codingchallenge.user.core.domain.models.User;
import ma.cires.codingchallenge.user.core.domain.models.UserProfile;

import java.util.List;

public interface UserRepository {
    List<User> generateUsers(Integer count);

    ImportUserStats importUserList(List<User> users);

    UserProfile getUser(String username);
}
