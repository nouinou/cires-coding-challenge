package ma.cires.codingchallenge.user.core.application;

import lombok.AllArgsConstructor;
import ma.cires.codingchallenge.user.core.domain.models.UserProfile;
import ma.cires.codingchallenge.user.core.domain.repositories.UserRepository;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class GetUserProfileService {

    private final UserRepository userRepository;

    public UserProfile run(String username) {
        return userRepository.getUser(username);
    }
}
