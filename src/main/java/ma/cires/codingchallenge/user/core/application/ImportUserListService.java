package ma.cires.codingchallenge.user.core.application;

import lombok.AllArgsConstructor;
import ma.cires.codingchallenge.user.core.domain.models.ImportUserStats;
import ma.cires.codingchallenge.user.core.domain.models.User;
import ma.cires.codingchallenge.user.core.domain.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ImportUserListService {

    private final UserRepository userRepository;

    public ImportUserStats run(List<User> userList) {
        return userRepository.importUserList(userList);
    }
}
