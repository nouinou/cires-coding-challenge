package ma.cires.codingchallenge.user.core.utils;

import com.github.javafaker.Faker;
import ma.cires.codingchallenge.user.core.domain.Roles;

import java.util.Random;

public class UserGeneratorUtil extends Faker {
    private final Roles[] roles = Roles.values();
    private final Random random = new Random();

    // Return a random role
    public Roles role() {
        return roles[random.nextInt(roles.length)];
    }
}
