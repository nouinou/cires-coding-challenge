package ma.cires.codingchallenge.user.core.domain.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImportUserStats {
    private Integer totalGenerated;
    private Integer successfullyImported;
    private Integer notImported;
}
