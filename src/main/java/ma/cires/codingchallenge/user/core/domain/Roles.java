package ma.cires.codingchallenge.user.core.domain;

public enum Roles {
    ADMIN,
    USER
}
