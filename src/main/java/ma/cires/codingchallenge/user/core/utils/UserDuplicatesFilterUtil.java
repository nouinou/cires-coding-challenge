package ma.cires.codingchallenge.user.core.utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

public class UserDuplicatesFilterUtil {
    public static <T> Predicate<T> filterByKey(
            Function<? super T, ?> extractor) {
        Map<Object, Boolean> hashMap = new ConcurrentHashMap<>();

        // Return true if the key was found in the HashMap => it will be filtered
        return t -> hashMap.putIfAbsent(extractor.apply(t), Boolean.TRUE) == null;
    }
}
