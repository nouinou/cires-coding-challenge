package ma.cires.codingchallenge.user.core.application;

import lombok.AllArgsConstructor;
import ma.cires.codingchallenge.user.core.domain.models.User;
import ma.cires.codingchallenge.user.core.domain.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class GenerateUserService {

    private final UserRepository userRepository;

    public List<User> run(Integer count) {
        return userRepository.generateUsers(count);
    }
}
