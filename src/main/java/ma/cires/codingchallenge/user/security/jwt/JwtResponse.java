package ma.cires.codingchallenge.user.security.jwt;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class JwtResponse implements Serializable {
    // This will change the response from {"token": "{generatedToken}"}
    // To {"accessToken": "{generatedToken}"}
    private final String accessToken;
}
