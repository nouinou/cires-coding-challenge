package ma.cires.codingchallenge.user.ports.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.AllArgsConstructor;
import ma.cires.codingchallenge.user.core.application.GenerateUserService;
import ma.cires.codingchallenge.user.core.application.GetUserProfileService;
import ma.cires.codingchallenge.user.core.application.ImportUserListService;
import ma.cires.codingchallenge.user.core.domain.Roles;
import ma.cires.codingchallenge.user.core.domain.models.ImportUserStats;
import ma.cires.codingchallenge.user.core.domain.models.User;
import ma.cires.codingchallenge.user.core.domain.models.UserProfile;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("api/users")
public class UserController {

    private GenerateUserService generateUserService;
    private ImportUserListService importUserListService;
    private GetUserProfileService getUserProfileService;

    @GetMapping("/generate")
    public ResponseEntity<List<User>> generateUsersList(@RequestParam Integer count) {
        List<User> users = generateUserService.run(count);

        // Naming the generated file
        String fileNameAttachmentTemplate = "attachment; filename=\"%s_cires-tech_%s_user.json\"";
        String fileNamAttachment = String.format(fileNameAttachmentTemplate, new Date().getTime(), count);

        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.CONTENT_DISPOSITION, fileNamAttachment)
                .body(users);
    }

    @PostMapping(value = "/batch", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<ImportUserStats> importUsers(@RequestParam(value = "users") MultipartFile userListFile) throws IOException {

        try {
            // Getting a stream from the uploaded file
            InputStream inputStream = userListFile.getInputStream();

            // Create a user list from the stream
            List<User> userList = new ObjectMapper().readValue(inputStream, new TypeReference<List<User>>() {
            });

            // Execute the service
            ImportUserStats importStatistics = importUserListService.run(userList);
            return ResponseEntity.ok(importStatistics);
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    // Security requirement in SecurityConfigurer to make Swagger send access token in headers
    @Operation(security = @SecurityRequirement(name = "bearer-key"))
    @GetMapping("/me")
    public ResponseEntity<UserProfile> getCurrentUserProfile(Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        String username = userDetails.getUsername();
        UserProfile user = getUserProfileService.run(username);

        return ResponseEntity.ok().body(user);
    }

    @Operation(security = @SecurityRequirement(name = "bearer-key"))
    @GetMapping("/{requestedUsername}")
    public ResponseEntity<UserProfile> getUserProfileV2(Authentication authentication, @PathVariable String requestedUsername) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        boolean isCurrentUserAdmin = userDetails
                .getAuthorities()
                .stream()
                .anyMatch(a -> a.getAuthority().equals(Roles.ADMIN.name()));

        UserProfile requestedUser = getUserProfileService.run(requestedUsername);
        String authenticatedUserEmail = userDetails.getUsername();

        UserProfile authenticatedUser = getUserProfileService.run(authenticatedUserEmail);
        String authenticatedUserUsername = authenticatedUser.getUsername();

        if (isCurrentUserAdmin) {
            // If Admin show the user if found
            if (requestedUser != null) {
                return ResponseEntity.ok().body(requestedUser);
            } else {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            }
        } else {
            // Check if the authenticated user is the same user by username
            if (authenticatedUserUsername.equals(requestedUsername) || requestedUsername.equals("me")) {
                // Since the user is in fact authenticated, it means that the requestedUser is not null
                return ResponseEntity.ok().body(requestedUser);
            }
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
    }
}
