package ma.cires.codingchallenge.user.ports.infrastructure.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ma.cires.codingchallenge.user.core.domain.models.User;
import ma.cires.codingchallenge.user.core.domain.models.UserProfile;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "USER")
@AllArgsConstructor
@NoArgsConstructor
public class UserEntity {
    @Id
    @GeneratedValue
    private Long id;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private String city;
    private String country;
    private String avatar;
    private String company;
    private String jobPosition;
    private String mobile;
    private String username;
    private String email;
    private String password;
    private String role;

    public UserProfile mapToUserProfile() {
        return new UserProfile(
                this.firstName,
                this.lastName,
                this.birthDate,
                this.city,
                this.country,
                this.avatar,
                this.company,
                this.jobPosition,
                this.mobile,
                this.username,
                this.email,
                this.role
        );
    }

    public static UserEntity mapFromUser(User user) {
        return new UserEntity(
                null,
                user.getFirstName(),
                user.getLastName(),
                user.getBirthDate(),
                user.getCity(),
                user.getCountry(),
                user.getAvatar(),
                user.getCompany(),
                user.getJobPosition(),
                user.getMobile(),
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                user.getRole()
        );
    }
}
