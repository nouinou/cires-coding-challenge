package ma.cires.codingchallenge.user.ports.infrastructure.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserEntityRepository extends JpaRepository<UserEntity, String> {
    @Query("select u from UserEntity u where u.username = ?1 or u.email = ?1")
    UserEntity findByUsernameOrEmail(String usernameOrEmail);
}
