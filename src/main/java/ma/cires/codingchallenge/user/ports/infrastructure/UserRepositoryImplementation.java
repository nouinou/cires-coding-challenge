package ma.cires.codingchallenge.user.ports.infrastructure;

import lombok.AllArgsConstructor;
import ma.cires.codingchallenge.user.core.domain.models.ImportUserStats;
import ma.cires.codingchallenge.user.core.domain.models.User;
import ma.cires.codingchallenge.user.core.domain.models.UserProfile;
import ma.cires.codingchallenge.user.core.domain.repositories.UserRepository;
import ma.cires.codingchallenge.user.core.utils.UserDuplicatesFilterUtil;
import ma.cires.codingchallenge.user.core.utils.UserGeneratorUtil;
import ma.cires.codingchallenge.user.ports.infrastructure.data.UserEntity;
import ma.cires.codingchallenge.user.ports.infrastructure.data.UserEntityRepository;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
@AllArgsConstructor
public class UserRepositoryImplementation implements UserRepository, UserDetailsService {

    private final UserEntityRepository userEntityRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public List<User> generateUsers(Integer count) {
        List<User> users = new ArrayList<>();

        UserGeneratorUtil generator = new UserGeneratorUtil();

        // add {count} users to the user ArrayList
        int i = 0;
        do {
            users.add(new User(
                    generator.name().firstName(),
                    generator.name().lastName(),
                    generator.date().birthday(),
                    generator.address().city(),
                    generator.address().countryCode(),
                    generator.avatar().image(),
                    generator.company().name(),
                    generator.job().position(),
                    generator.phoneNumber().cellPhone(),
                    generator.name().username(),
                    generator.internet().safeEmailAddress(),
                    generator.internet().password(6, 10, true, true, true),
                    generator.role().toString()
            ));

            i++;
        } while (i < count);

        return users;
    }

    @Override
    public ImportUserStats importUserList(List<User> userList) {
        List<UserEntity> userEntities = userList
                .stream()
                .map(user -> {
                    UserEntity userEntity = UserEntity.mapFromUser(user);
                    userEntity.setPassword(passwordEncoder.encode(userEntity.getPassword()));
                    return userEntity;
                })
                .filter(UserDuplicatesFilterUtil.filterByKey(u -> u.getUsername()))
                .filter(UserDuplicatesFilterUtil.filterByKey(u -> u.getEmail()))
                .collect(toList());

        List<UserEntity> importedUsers = userEntityRepository.saveAll(userEntities);
        return new ImportUserStats(userList.size(), importedUsers.size(), userList.size() - importedUsers.size());
    }

    @Override
    public UserProfile getUser(String username) {
         UserEntity userEntity = userEntityRepository.findByUsernameOrEmail(username);
         return userEntity != null ? userEntity.mapToUserProfile() : null;
    }

    // This will be use for authentication
    // It's called load user by username from the UserDetailsService interface, but (step3) requires either username or email
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // Optional because it might be null
        Optional<UserEntity> user = Optional.ofNullable(userEntityRepository.findByUsernameOrEmail(username));

        if(user.isPresent()) {
            UserEntity foundUser = user.get();
            Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority(foundUser.getRole()));

            // Returning the userDetail for authentication
            return new org.springframework.security.core.userdetails.User(foundUser.getEmail(), foundUser.getPassword(), authorities);
        } else {
            throw new UsernameNotFoundException("Username or Email not found");
        }
    }

}
