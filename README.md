## Cires Coding Challenge

### This technical coding challenge project is building a backend API that allows to:

- Generate and download a JSON file containing a list of users.
- Import the generated list above and into an H2 in memory Database with an encrypted password.
- Authenticate a User with ausername or an email and the password using JWT.
- Get the authenticated user's profile.
- Get a user profile by it's username if the authenticated user's role is ADMIN.


## Libraries/Tools used

- Written in Java. Tested on Java8. 
- Maven for build automation
- Spring Boot Framework
- Lombok for automating getters and construstors
- H2 database for in memory dabatase
- AssertJ for unit testing
- JavaFaker for generating random data
- Java Persistence API for describing Entities
- JJWT for JWT authentication
- Springdoc Openapi to document and test endpoints with swagger
- Flyway plugin for database migration

## Endpoints

- `GET /api/users/generate?count={count}` generate `count` users and download it (No authorization required).
- `POST /api/users/batch` import the users in JSON file to the database (No authorization required).
- `POST /api/auth` authenticate the user
- `GET /api/users/me` check authenticated user's profile (Authorization required).
- `GET /api/users/{username}` check `username` profile (Authorization required).

Endpoints are documented in and testable via the Swagger Interface: `http://localhost:9090/swagger-ui.html`

## Running the project and tests

### Without IDE
After pulling the repository, make sure Java8 and Maven are installed. Then use maven to run it:
`mvn clean install` to install dependencies and run unit tests
`mvn test` to run unit test
`mvn spring-boot:run` to run the project

### With IDE
After installing the dependencies using maven plugin you can run `UsersApplication.java` to run the project. 
For testing, you can use the maven plugin to run all tests or run them manually separatly.

